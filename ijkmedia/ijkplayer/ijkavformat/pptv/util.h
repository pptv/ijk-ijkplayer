//
// Created by sizhenzhu on 2018/9/13.
//

#ifndef JUST_UTIL_H
#define JUST_UTIL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <libavformat/avformat.h>
#include <libavutil/log.h>

#define VIDEO_STREAM_INDEX 0
#define AUDIO_STREAM_INDEX 1

#define AVSEEK_FLAG_FOR_SWITCH 1024

#define UTIL_LOG_DEBUG(avcl, fmt, ...) av_log(avcl, AV_LOG_DEBUG, fmt, ##__VA_ARGS__)
#define UTIL_LOG_INFO(avcl, fmt, ...) av_log(avcl, AV_LOG_INFO, fmt, ##__VA_ARGS__)
#define UTIL_LOG_WARNING(avcl, fmt, ...) av_log(avcl, AV_LOG_WARNING, fmt, ##__VA_ARGS__)
#define UTIL_LOG_ERROR(avcl, fmt, ...) av_log(avcl, AV_LOG_ERROR, fmt, ##__VA_ARGS__)

#define millseconds_to_fftime(ms) (av_rescale(ms, AV_TIME_BASE, 1000))
#define fftime_to_milliseconds(ts) (av_rescale(ts, 1000, AV_TIME_BASE))

int find_video_stream_index(AVFormatContext* format_context);
int find_audio_stream_index(AVFormatContext* format_context);

#ifdef __cplusplus
}
#endif

#endif //JUST_UTIL_H
