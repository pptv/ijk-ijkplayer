#include "SegmentDemux.h"
#include "ijkavformat.h"

#include <limits.h>
#include <stdint.h>
#include <string>
#ifndef INT64_MIN
#define INT64_MIN (-9223372036854775807LL - 1)
#endif
#ifndef INT64_MAX
#define INT64_MAX 9223372036854775807LL
#endif

#include "util.h"

namespace pptv {

SegmentDemux::SegmentDemux(Segment current_segment, AVFormatContext* prev_context, FFPlayer *ffplayer)
{
    segment = current_segment;
    url = current_segment.url;
    wrap_format_context = prev_context;
    time_offset_ms = current_segment.start_time_ms;
    format_context = NULL;
    audio_stream_index = -1;
    video_stream_index = -1;
    video_pkt_flag_set = false;
    audio_pkt_flag_set = false;
    ffp = ffplayer;
    open_opts = NULL;
}

SegmentDemux::~SegmentDemux()
{
    if (format_context) {
        avformat_close_input(&format_context);
        format_context = NULL;
    }
    if (open_opts) {
        av_dict_free(&open_opts);
        open_opts = NULL;
    }
}

int64_t SegmentDemux::cal_offset(int stream_index)
{
    AVRational time_base = format_context->streams[stream_index]->time_base;
    int64_t offset = av_rescale_rnd(time_offset_ms, time_base.den, time_base.num * 1000, AV_ROUND_UP);
    UTIL_LOG_INFO(NULL, "[%s] stream_index %d, time_base %f, offset %lld", __func__, stream_index, av_q2d(time_base), offset);
    return offset;
}

bool SegmentDemux::open()
{
    return open_input();
}

bool SegmentDemux::open_input()
{
    format_context = avformat_alloc_context();
    AVInputFormat* input_format = NULL;
    format_context->interrupt_callback = wrap_format_context->interrupt_callback;
    if (open_opts == NULL) {
        if (av_dict_copy(&open_opts, ffp->format_opts, 0) != 0) {
            open_opts = NULL;
        }
    }
    if (segment.is_live) {
        format_context->skip_initial_bytes = segment.start_offset;
        input_format = av_find_input_format("flv");
        if (open_opts != NULL) {
            UTIL_LOG_INFO(NULL, "remove 'reconnect' option for flv.\n");
            av_dict_set(&open_opts, "reconnect", NULL, 0);
        }
    } else {
        input_format = av_find_input_format("mov,mp4,m4a,3gp,3g2,mj2");
        if (input_format) {
            input_format->flags |= AVFMT_NOGENSEARCH;
        }
    }

    int ret = avformat_open_input(&format_context, url.c_str(), input_format, &open_opts);
    if (ret != 0) {
        UTIL_LOG_ERROR(NULL, "[%s] avformat open failed, %s\n", __func__, url.c_str());
        return false;
    }
    ret = avformat_find_stream_info(format_context, NULL);
    if (ret < 0) {
        UTIL_LOG_ERROR(NULL, "[%s] avformat_find_stream_info failed, %s\n", __func__, url.c_str());
        return false;
    }
    for (int i = 0; i < format_context->nb_streams; ++i) {
        if (format_context->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
            video_stream_index = i;
        }
        if (format_context->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) {
            audio_stream_index = i;
        }
    }
    if (audio_stream_index == -1 || video_stream_index == -1) {
        UTIL_LOG_ERROR(NULL, "[%s] video_stream_index %d, audio_stream_index %d failed\n",
            __func__, video_stream_index, audio_stream_index);
        return false;
    }
    UTIL_LOG_INFO(NULL, "[%s] audio_stream_index %d, video_stream_index %d\n",
        __func__, audio_stream_index, video_stream_index);
    video_offset = cal_offset(video_stream_index);
    audio_offset = cal_offset(audio_stream_index);
    UTIL_LOG_INFO(NULL, "[%s] video_offset %lld, audio_offset %lld\n", __func__, video_offset, audio_offset);
    return true;
}

int SegmentDemux::read_packet(AVPacket* pkt)
{
    if (segment.is_live) {
        return read_live_packet(pkt);
    }

    return read_vod_packet(pkt);
}

int SegmentDemux::read_live_packet(AVPacket* pkt)
{
    // TODO key超时处理
    // TODO 等待一段时间后再重试
    if (!format_context) {
        if (!open_input()) {
            avformat_close_input(&format_context);
            format_context = NULL;
            UTIL_LOG_WARNING(NULL, "[%s] open failed, try again", __func__);
            return AVERROR(EAGAIN);
        }
    }

    return read_packet_from_format_context(pkt);
}

int SegmentDemux::read_vod_packet(AVPacket* pkt)
{
    return read_packet_from_format_context(pkt);
}

int SegmentDemux::read_packet_from_format_context(AVPacket* pkt)
{
    int ret = 0;
    if (format_context == NULL) {
        return AVERROR_EXIT;
    }
    ret = av_read_frame(format_context, pkt);
    if (ret == 0) {
        add_side_data(pkt);
        adjust_pts_and_stream_index(pkt);
    }
    return ret;
}

/**
*   调用该函数后pkt->stream_index可能改变
*/
void SegmentDemux::adjust_pts_and_stream_index(AVPacket* pkt)
{
    AVRational wrap_time_base;
    int old_stream_index = pkt->stream_index;
    if (pkt->stream_index == video_stream_index) {
        pkt->pts += video_offset;
        pkt->stream_index = VIDEO_STREAM_INDEX;
        wrap_time_base = wrap_format_context->streams[VIDEO_STREAM_INDEX]->time_base;
    } else if (pkt->stream_index == audio_stream_index) {
        pkt->pts += audio_offset;
        pkt->stream_index = AUDIO_STREAM_INDEX;
        wrap_time_base = wrap_format_context->streams[AUDIO_STREAM_INDEX]->time_base;
    }
    if (av_cmp_q(wrap_time_base, format_context->streams[old_stream_index]->time_base) != 0) {
        pkt->pts = av_rescale_q(pkt->pts, format_context->streams[old_stream_index]->time_base, wrap_time_base);
        pkt->duration = av_rescale_q(pkt->duration, format_context->streams[old_stream_index]->time_base, wrap_time_base);
    }
}

void SegmentDemux::add_side_data(AVPacket* pkt)
{
    if (pkt->stream_index == video_stream_index && video_pkt_flag_set) {
        return;
    }
    if (pkt->stream_index == audio_stream_index && audio_pkt_flag_set) {
        return;
    }
    uint8_t* pkt_extradata = NULL;
    int pkt_extradata_size = 0;
    av_packet_get_side_data(pkt, AV_PKT_DATA_NEW_EXTRADATA, &pkt_extradata_size);
    if (pkt_extradata && pkt_extradata_size > 0) {
        UTIL_LOG_WARNING(NULL, "[%s] extradata already set", __func__);
        set_extradata_sent(pkt->stream_index, true);
        return;
    }

    uint8_t* stream_extradata = format_context->streams[pkt->stream_index]->codecpar->extradata;
    int stream_extradata_size = format_context->streams[pkt->stream_index]->codecpar->extradata_size;
    uint8_t* new_extradata = av_packet_new_side_data(pkt, AV_PKT_DATA_NEW_EXTRADATA, stream_extradata_size);
    if (!new_extradata) {
        UTIL_LOG_WARNING(NULL, "[%s] av_packet_new_side_data failed", __func__);
        return;
    }
    memcpy(new_extradata, stream_extradata, stream_extradata_size);

    set_extradata_sent(pkt->stream_index, true);
    UTIL_LOG_INFO(NULL, "[%s] stream %d set extradata, size %d", __func__, pkt->stream_index, stream_extradata_size);
}

void SegmentDemux::set_extradata_sent(int stream_index, bool sent)
{
    if (stream_index == video_stream_index) {
        video_pkt_flag_set = sent;
    } else if (stream_index == audio_stream_index) {
        audio_pkt_flag_set = sent;
    }
}

AVFormatContext* SegmentDemux::get_format_context()
{
    return format_context;
}

int SegmentDemux::seek(int stream_index, int64_t time_ms, int flags)
{
    if (segment.is_live) {
        UTIL_LOG_INFO(NULL, "[%s] is_live", __func__);
        return 0;
    }
    int64_t seek_time_in_timebase = millseconds_to_fftime(time_ms);
    if (seek_time_in_timebase > format_context->duration)
    {
        UTIL_LOG_WARNING(NULL, "[%s] adjust seek_time_in_timebase, seek_time %lld, duration %lld", __func__, seek_time_in_timebase, format_context->duration);
        seek_time_in_timebase = format_context->duration - 1000;
    }
    UTIL_LOG_INFO(NULL, "[%s] stream_index %d, time_ms %lld, seek_time_in_timebase %lld, flags %d\n", __func__, stream_index, time_ms, seek_time_in_timebase, flags);
    int ret = 0;
    // 切换画质使用av_seek_frame，只向前seek
    if (flags & AVSEEK_FLAG_FOR_SWITCH){
        ret = av_seek_frame(format_context, -1, seek_time_in_timebase, 0);
        UTIL_LOG_INFO(NULL, "[%s] av_seek_frame ret %d\n", __func__, ret);
    } else {
        ret = avformat_seek_file(format_context, -1, 0, seek_time_in_timebase, format_context->duration, flags);
        UTIL_LOG_INFO(NULL, "[%s] avformat_seek_file ret %d\n", __func__, ret);
    }
    set_extradata_sent(VIDEO_STREAM_INDEX, false);
    set_extradata_sent(AUDIO_STREAM_INDEX, false);
    return ret;
}

AVRational SegmentDemux::video_timebase()
{
    return wrap_format_context->streams[VIDEO_STREAM_INDEX]->time_base;
}

AVRational SegmentDemux::audio_timebase()
{
    return wrap_format_context->streams[AUDIO_STREAM_INDEX]->time_base;
}
}