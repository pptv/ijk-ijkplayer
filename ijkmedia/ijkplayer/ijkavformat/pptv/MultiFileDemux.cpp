//
// Created by sizhenzhu on 2018/9/12.
//

#include "MultiFileDemux.h"
#include <framework/string/Url.h>

#include <boost/lexical_cast.hpp>

#include "util.h"

namespace pptv {

SwitchInfo::SwitchInfo()
{
    demux = NULL;
    pkt = NULL;
}

SwitchInfo::~SwitchInfo()
{
    if (demux) {
        delete demux;
    }
    if (pkt) {
        av_packet_free(&pkt);
    }
}

MultiFileDemux::MultiFileDemux(char* url, char* play_xml_content, AVFormatContext* context, int port, FFPlayer *ffplayer)
    : play_xml(play_xml_content, url, port)
    , wrap_format_context(context)
    , file_demux(NULL)
    , switching_info(NULL)
    , next_switch_info(NULL)
    , ffp(ffplayer)
{
    framework::string::Url tmp_url(url);
    rid = tmp_url.param("rid");
    ft = tmp_url.param("ft");
}

MultiFileDemux::~MultiFileDemux()
{
    if (file_demux) {
        delete file_demux;
        file_demux = NULL;
    }
    if (switching_info) {
        delete switching_info;
        switching_info = NULL;
    }
    if (next_switch_info) {
        delete next_switch_info;
        next_switch_info = NULL;
    }
}

bool MultiFileDemux::open()
{
    if (!play_xml.vaild()) {
        UTIL_LOG_ERROR(NULL, "[%s] play_xml not valid\n", __func__);
        return false;
    }

    if (ft.empty() || rid.empty()) {
        UTIL_LOG_ERROR(NULL, "[%s] ft or rid not valid, ft %s, rid %s\n", __func__, ft.c_str(), rid.c_str());
        return false;
    }

    FileItem* file_item = play_xml.select_file(ft, rid);
    if (file_item->rid.empty()) {
        UTIL_LOG_ERROR(NULL, "[%s] select_file fail\n", __func__);
        return false;
    }
    // set duration
    wrap_format_context->duration = millseconds_to_fftime(play_xml.get_duration_ms());
    UTIL_LOG_INFO(NULL, "[%s] play_xml.duration %d, wrap_format_context duration %lld", __func__, play_xml.get_duration_ms(),
        wrap_format_context->duration);
    file_demux = new FileDemux(file_item, wrap_format_context, ffp);
    return file_demux->open();
}

int MultiFileDemux::read_packet(AVPacket* pkt)
{
    if (switching_info && switching_info->rid == rid && switching_info->ft == ft) {
        UTIL_LOG_INFO(NULL, "[%s] same ft and rid, ignore switch", __func__);
        delete switching_info;
        switching_info = NULL;
    }
    if (!switching_info && next_switch_info) {
        UTIL_LOG_INFO(NULL, "[%s] set switching_info to next_switch_info", __func__);
        switching_info = next_switch_info;
        next_switch_info = NULL;
    }
    if (!switching_info) {
        return file_demux->read_packet(pkt);
    }
    if (!switching_info->demux) {
        UTIL_LOG_INFO(NULL, "[%s] select_file ft %s, rid %s ", __func__, switching_info->ft.c_str(), switching_info->rid.c_str());
        FileItem* new_item = play_xml.select_file(switching_info->ft, switching_info->rid);
        new_item->set_current_index(-1);
        switching_info->demux = new FileDemux(new_item, wrap_format_context, ffp);
        if (!switching_info->demux->open()) {
            UTIL_LOG_ERROR(NULL, "[%s] demux open failed", __func__);
            ffp_notify_msg4(ffp, FFP_MSG_PPTV_SWITCH_RESULT, 0, cast_to_int(switching_info->ft), (void *)switching_info->rid.c_str(), strlen(switching_info->rid.c_str()));
            delete switching_info;
            switching_info = NULL;
            return AVERROR(EAGAIN);
        }
        int switch_ret = 0;
        uint64_t last_time_ms = FFMAX(file_demux->last_video_time_ms(), file_demux->last_audio_time_ms());
        UTIL_LOG_INFO(NULL, "[%s] last_time_ms %lld", __func__, last_time_ms);
        switch_ret = seek_for_switch(last_time_ms, switching_info);
        if (switch_ret >= 0) {
            UTIL_LOG_INFO(NULL, "[%s] set switch_time_ms %lld", __func__, switching_info->switch_time_ms);
        } else {
            UTIL_LOG_ERROR(NULL, "[%s] switching_info->file_demux seek failed", __func__);
            ffp_notify_msg4(ffp, FFP_MSG_PPTV_SWITCH_RESULT, 0, cast_to_int(switching_info->ft), (void *)switching_info->rid.c_str(), strlen(switching_info->rid.c_str()));
            delete switching_info;
            switching_info = NULL;
            return AVERROR(EAGAIN);
        }
    }
    if (file_demux->last_video_time_ms() >= switching_info->switch_time_ms && file_demux->last_audio_time_ms() >= switching_info->switch_time_ms) {
        UTIL_LOG_INFO(NULL, "[%s] audio and video all reach switch_time_ms", __func__);
        av_packet_ref(pkt, switching_info->pkt);
        av_packet_unref(switching_info->pkt);
        delete file_demux;
        file_demux = NULL;
        file_demux = switching_info->demux;
        switching_info->demux = NULL;
        ft = switching_info->ft;
        rid = switching_info->rid;
        // pkt->flags |= AV_PKT_FLAG_DEMUX_SWITCHED;
        UTIL_LOG_INFO(NULL, "[%s] set switching_info NULL", __func__);
        ffp_notify_msg4(ffp, FFP_MSG_PPTV_SWITCH_RESULT, 1, cast_to_int(ft), (void *)rid.c_str(), strlen(rid.c_str())+1);
        delete switching_info;
        switching_info = NULL;
        return 0;
    } else {
        int read_ret = file_demux->read_packet(pkt);
        if (read_ret != 0) {
            return read_ret;
        }
        if ((pkt->stream_index == VIDEO_STREAM_INDEX) && (file_demux->last_video_time_ms() > switching_info->switch_time_ms)) {
            av_packet_unref(pkt);
            return AVERROR(EAGAIN);
        }
        if ((pkt->stream_index == AUDIO_STREAM_INDEX) && (file_demux->last_audio_time_ms() > switching_info->switch_time_ms)) {
            av_packet_unref(pkt);
            return AVERROR(EAGAIN);
        }
        return read_ret;
    }
}

int MultiFileDemux::seek_for_switch(uint64_t target_time_ms, SwitchInfo *switching_info)
{
    int seek_ret = 0;
    uint64_t seek_time_ms = target_time_ms;
    // 直播流可能出现不同画质的同一个block对应的时间不一致，需要seek到下一个block
    for (int i = 0; i < 3; ++i) {
        seek_ret = switching_info->demux->seek(-1, seek_time_ms, AVSEEK_FLAG_FOR_SWITCH);
        if (seek_ret >= 0) {
            switching_info->pkt = av_packet_alloc();
            switching_info->demux->read_packet(switching_info->pkt);
            UTIL_LOG_INFO(NULL, "[%s] read_packet pts %lld", __func__, switching_info->pkt->pts);
            if (switching_info->pkt->stream_index == VIDEO_STREAM_INDEX) {
                switching_info->switch_time_ms = switching_info->demux->last_video_time_ms();
            } else {
                switching_info->switch_time_ms = switching_info->demux->last_audio_time_ms();
            }
            UTIL_LOG_INFO(NULL, "[%s] set switch_time_ms %lld", __func__, switching_info->switch_time_ms);
            if (switching_info->switch_time_ms < target_time_ms) {
                seek_time_ms += 5000;
                av_packet_free(&switching_info->pkt);
                switching_info->pkt = NULL;
                UTIL_LOG_WARNING(NULL, "[%s] switch_time_ms %lld, target_time_ms %lld, retry", __func__, switching_info->switch_time_ms, target_time_ms);
                continue;
            } else {
                return 1;
            }
        } else {
            UTIL_LOG_ERROR(NULL, "[%s] switching_info->file_demux seek failed", __func__);
            return -1;
        }
    }
    return -1;
}

int MultiFileDemux::seek(int stream_index, int64_t ts, int flags)
{
    if (ts < 0) {
        UTIL_LOG_WARNING(NULL, "[%s] adjust ts %lld to 0", __func__, ts);
        ts = 0;
    }
    UTIL_LOG_INFO(NULL, "[%s] stream_index %d, ts %lld", __func__, stream_index, ts);
    int64_t seek_time_ms = 0;
    if (stream_index < 0 || stream_index >= wrap_format_context->nb_streams) {
        seek_time_ms = fftime_to_milliseconds(ts);
    } else {
        AVRational time_base = wrap_format_context->streams[stream_index]->time_base;
        seek_time_ms = (ts * av_q2d(time_base)) * 1000;
    }
    UTIL_LOG_INFO(NULL, "[%s] stream_index %d, seek_time_ms %lld", __func__, stream_index, seek_time_ms);
    return file_demux->seek(-1, seek_time_ms, flags);
}

AVFormatContext* MultiFileDemux::get_format_context()
{
    return file_demux->get_format_context();
}

bool MultiFileDemux::switch_stream(char* url)
{
    framework::string::Url tmp_url(url);
    std::string rid = tmp_url.param("rid");
    std::string ft = tmp_url.param("ft");
    if (switching_info) {
        UTIL_LOG_INFO(NULL, "[%s] switching, set next_switch_info", __func__);
        if (next_switch_info) {
            UTIL_LOG_INFO(NULL, "[%s] delete next_switch_info, ft %s, rid %s", __func__, ft.c_str(), rid.c_str());
            delete next_switch_info;
            next_switch_info = NULL;
        }
        next_switch_info = new SwitchInfo();
        next_switch_info->ft = ft;
        next_switch_info->rid = rid;
        next_switch_info->url = tmp_url;
    } else {
        switching_info = new SwitchInfo();
        switching_info->ft = ft;
        switching_info->rid = rid;
        switching_info->url = tmp_url;
    }
    return true;
}

int MultiFileDemux::cast_to_int(std::string val)
{
    int result = -1;
    try {
        result = boost::lexical_cast<int>(val);
    } catch (boost::bad_lexical_cast& e) {
        UTIL_LOG_WARNING(NULL, "[%s] failed, val %s", __func__, val.c_str());
        result = -1;
    }
    return result;
}
}

void* new_multi_file_demux(char* url, char* play_xml_content, AVFormatContext* context, int port, FFPlayer *ffp)
{
    pptv::MultiFileDemux* demux = new pptv::MultiFileDemux(url, play_xml_content, context, port, ffp);
    return (void*)demux;
}

void delete_multi_file_demux(void* demux)
{
    UTIL_LOG_INFO(NULL, "[%s]\n", __func__);
    pptv::MultiFileDemux* file_demux = (pptv::MultiFileDemux*)demux;
    delete file_demux;
}

int multi_file_demux_open(void* demux)
{
    UTIL_LOG_INFO(NULL, "[%s]\n", __func__);
    pptv::MultiFileDemux* file_demux = (pptv::MultiFileDemux*)demux;
    if (file_demux->open()) {
        return 0;
    }
    return 1;
}

int multi_file_demux_read_packet(void* demux, AVPacket* pkt)
{
    pptv::MultiFileDemux* file_demux = (pptv::MultiFileDemux*)demux;
    return file_demux->read_packet(pkt);
}

int multi_file_demux_seek(void* demux, int stream_index, int64_t time_ms, int flags)
{
    pptv::MultiFileDemux* file_demux = (pptv::MultiFileDemux*)demux;
    UTIL_LOG_INFO(NULL, "[%s] file_demux %p, stream_index %d, time %lld\n", __func__, file_demux, stream_index, time_ms);
    if (!file_demux) {
        UTIL_LOG_ERROR(NULL, "[%s] file_demux is null\n", __func__);
        return -1;
    }
    return file_demux->seek(stream_index, time_ms, flags);
}

AVFormatContext* multi_file_demux_get_format_context(void* demux)
{
    pptv::MultiFileDemux* file_demux = (pptv::MultiFileDemux*)demux;
    return file_demux->get_format_context();
}

int multi_file_demux_switch(void* demux, char* new_url)
{
    pptv::MultiFileDemux* file_demux = (pptv::MultiFileDemux*)demux;
    return file_demux->switch_stream(new_url);
}