//
// Created by sizhenzhu on 2018/8/23.
//

#include "PlayXml.h"
#include <algorithm>
#include <iostream>
#include <memory.h>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "pugixml/pugixml.hpp"
#include <anti-hotlinking-c.h>
#include <framework/string/Url.h>

#include <boost/lexical_cast.hpp>

using namespace pugi;

extern "C" {
#include "util.h"
#include <libavutil/log.h>
}

namespace pptv {
FileItem::FileItem(int port)
{
    p2p_port = port;
    current_index = -1;
    duration_ms = 0;
}

FileItem::~FileItem()
{
}

void FileItem::add_segment(pptv::Segment segment)
{
    segment.start_time_ms = duration_ms;
    duration_ms += segment.duration_ms;
    segment.index = segments.size();
    segment.is_live = false;
    segments.push_back(segment);
}

void FileItem::set_current_index(int index)
{
    current_index = index;
}

framework::string::Url FileItem::p2p_prepare(std::string cdn_url)
{
    framework::string::Url url = framework::string::Url();
    url.protocol("http");
    url.host("127.0.0.1");
    url.svc(boost::lexical_cast<std::string>(p2p_port));
    url.param("url", cdn_url);
    url.param("BWType", boost::lexical_cast<std::string>(dt.bwt));
    url.param("autoclose", "false");
    url.param("bakhost", dt.bh);
    return url;
}

VodFileItem::VodFileItem(int port)
    : FileItem(port)
{
    is_live = false;
}

VodFileItem::~VodFileItem()
{
}

bool VodFileItem::have_next_segment()
{
    return current_index + 1 < segments.size();
}

Segment VodFileItem::next_segment()
{
    Segment emptySegment;
    current_index++;
    UTIL_LOG_INFO(NULL, "[%s] current_index %d, segments.size %d\n", __func__, current_index, segments.size());
    if (current_index >= segments.size()) {
        UTIL_LOG_WARNING(NULL, "[%s] reach end\n", __func__);
        return emptySegment;
    }

    if (segments[current_index].url.empty()) {
        segments[current_index].url = segment_url(segments[current_index]);
    }

    return segments[current_index];
}

std::string VodFileItem::segment_url(pptv::Segment segment)
{
    av_log(NULL, AV_LOG_INFO, "[%s] p2p_port %d", __func__, p2p_port);
    framework::string::Url url = framework::string::Url();
    if (p2p_port > 0) {
        url = p2p_segment_url(segment);
    } else {
        url = cdn_segment_url(segment);
    }

    return url.to_string();
}

// http://127.0.0.1:port/ppvaplaybyopen?url=cdn_url&BWType=<dt.bwt>&autoclose=false&bakhost=<dt.bh>&filelength=<sgm.fs>&headlength=<sgm.hl>&drag=1&headonly=0&rid=<sgm.rid>
framework::string::Url VodFileItem::p2p_segment_url(pptv::Segment segment)
{
    framework::string::Url cdn_url = cdn_segment_url(segment);
    av_log(NULL, AV_LOG_INFO, "[%s] cdn_url %s", __func__, cdn_url.to_string().c_str());
    framework::string::Url p2p_url = FileItem::p2p_prepare(cdn_url.to_string());
    p2p_url.path("/ppvaplaybyopen");
    p2p_url.param("filelength", boost::lexical_cast<std::string>(segment.fs));
    p2p_url.param("headlength", boost::lexical_cast<std::string>(segment.hl));
    p2p_url.param("drag", "1");
    p2p_url.param("headonly", "0");
    p2p_url.param("rid", segment.rid);
    av_log(NULL, AV_LOG_INFO, "[%s] p2p_url %s", __func__, p2p_url.to_string().c_str());
    return p2p_url;
}

// htttp://<dt.sh>/<sgm.no>_<dt.rid>?k=k&ft=ft&rid=<dt.rid>
framework::string::Url VodFileItem::cdn_segment_url(pptv::Segment segment)
{
    framework::string::Url url = pptv_url;
    url.protocol("http");
    url.host(dt.sh);
    // 部分老片源的rid中包含特殊字符，比如10108318
    url.path("/" + segment.no + "_" + framework::string::Url::decode(rid));
    url.param_del("proxy");
    url.param("k", framework::string::Url::decode(dt.k_decrypted));
    url.param("ft", ft);
    url.param("rid", rid);
    return url;
}

Segment VodFileItem::find_segment(int64_t target_time_ms)
{
    int target_index = 0;
    for (size_t i = 0; i < segments.size(); ++i) {
        Segment segment = segments[i];
        if (target_time_ms >= segment.start_time_ms && target_time_ms <= segment.start_time_ms + segment.duration_ms) {
            UTIL_LOG_INFO(NULL, "[%s] return segment %lld - %lld", __func__, segment.start_time_ms, (segment.start_time_ms + segment.duration_ms));
            target_index = i;
            break;
        }
    }
    if (target_index >= segments.size()) {
        UTIL_LOG_INFO(NULL, "[%s] return last segment", __func__);
        target_index = segments.size() - 1;
    }
    if (segments[target_index].url.empty()) {
        segments[target_index].url = segment_url(segments[target_index]);
    }

    return segments[target_index];
}

LiveFileItem::LiveFileItem(int base, int interval_value, int delay_value, int port)
    : FileItem(port)
{
    basetime = base;
    interval = interval_value;
    delay = delay_value;
    is_live = true;
}

LiveFileItem::~LiveFileItem()
{
}

bool LiveFileItem::have_next_segment()
{
    return true;
}

Segment LiveFileItem::next_segment()
{
    if (p2p_port <= 0) {
        current_index++;
        return make_live_segment(current_index);
    }

    if (current_index < 0){
        current_index = 0;
        return make_live_segment(current_index);
    }
    int current_time = time(NULL);
    int64_t target_time_ms = (current_time - basetime - delay) * 1000;
    return find_segment(target_time_ms);
}

Segment LiveFileItem::find_segment(int64_t target_time_ms)
{
    int index = 0;
    int block = 0;
    int current_time;
    block = basetime + (target_time_ms / 1000);

    current_time = time(NULL);
    av_log(NULL, AV_LOG_INFO, "[%s] current_time %d", __func__, current_time);
    block = std::min(block, current_time);
    block = std::max(basetime, block);
    index = (block - basetime) / 5 + 1;
    av_log(NULL, AV_LOG_INFO, "[%s] target_time_ms %lld, block %d, index %d", __func__, target_time_ms, block, index);
    return make_live_segment(index);
}

Segment LiveFileItem::make_live_segment(int index)
{
    std::stringstream sstream;
    int block = basetime + index * 5;
    av_log(NULL, AV_LOG_INFO, "[%s] block %d", __func__, block);
    Segment segment;
    sstream << index;
    segment.no = sstream.str();
    segment.index = index;
    segment.duration_s = 5;
    segment.duration_ms = 5000;
    // not need to adjust pts in flv
    segment.start_time_ms = 0;
    segment.rid = rid;
    segment.url = segment_url(segment, block);
    if (p2p_port > 0) {
        segment.start_offset = 0;
    } else {
        segment.start_offset = 1400;
    }
    segment.is_live = true;

    av_log(NULL, AV_LOG_INFO, "[%s] framework::String::Url::decode(dt.k_decrypted) %s", __func__, framework::string::Url::decode(dt.k_decrypted).c_str());
    av_log(NULL, AV_LOG_INFO, "[%s] index %d, url %s", __func__, index, segment.url.c_str());
    return segment;
}

std::string LiveFileItem::segment_url(pptv::Segment segment, int block)
{
    av_log(NULL, AV_LOG_INFO, "[%s] p2p_port %d", __func__, p2p_port);
    framework::string::Url url = framework::string::Url();
    if (p2p_port > 0) {
        url = p2p_segment_url(segment, block);
    } else {
        url = cdn_segment_url(segment, block);
    }

    return url.to_string();
}

// http://127.0.0.1:port/live/playlive.flv?url=cdn_url&BWType=<dt.bwt>&autoclose=false&bakhost=<dt.bh>&channelid=<dt.rid>&rid=<dt.rid>&datarate=<stream.item.bitrate>&interval=<stream.item.interval>&replay=0&start=timestamp&source=0&uniqueid
framework::string::Url LiveFileItem::p2p_segment_url(pptv::Segment segment, int block)
{
    framework::string::Url cdn_url = cdn_segment_url(segment, block);
    av_log(NULL, AV_LOG_INFO, "[%s] cdn_url %s", __func__, cdn_url.to_string().c_str());
    cdn_url.path("/live/");
    framework::string::Url p2p_url = FileItem::p2p_prepare(cdn_url.to_string());
    p2p_url.path("/playlive.flv");
    p2p_url.param("channelid", rid);
    p2p_url.param("rid", rid);
    p2p_url.param("datarate", boost::lexical_cast<std::string>(bitrate)); // kbps
    p2p_url.param("interval", boost::lexical_cast<std::string>(interval));
    p2p_url.param("replay", "0");
    p2p_url.param("start", boost::lexical_cast<std::string>(block));
    p2p_url.param("source", "0");
    p2p_url.param("uniqueid", boost::lexical_cast<std::string>(++seq));
    av_log(NULL, AV_LOG_INFO, "[%s] p2p_url %s", __func__, p2p_url.to_string().c_str());
    return p2p_url;
}

// http://<dt.sh>/live/timestamp.block?k=k&video=true
framework::string::Url LiveFileItem::cdn_segment_url(pptv::Segment segment, int block)
{
    framework::string::Url url = pptv_url;
    url.protocol("http");
    url.host(dt.sh);
    url.path("/live/" + rid + "/" + boost::lexical_cast<std::string>(block) + ".block");
    url.param_del("proxy");
    url.param("k", framework::string::Url::decode(dt.k_decrypted));
    url.param("video", "true");
    return url;
}

PlayXml::PlayXml(const char* xml, const char* url, int port)
{
    pptv_url.from_string(url);
    p2p_port = port;

    xml_document document;
    std::stringstream xml_stream;
    xml_stream << xml;

    parse_success = true;
    is_live = false;
    xml_parse_result result = document.load(xml_stream);

    int interval = 5;
    int delay = 45;

    if (!result) {
        parse_success = false;
        UTIL_LOG_ERROR(NULL, "[%s] load failed\n", __func__);
        return;
    }

    xml_node root_node = document.document_element();
    xml_node channel_node = root_node.select_node("/root/channel").node();
    duration_ms = channel_node.attribute("dur").as_int(0) * 1000;
    video_type = channel_node.attribute("vt").as_int(3);
    if (video_type == 0 || video_type == 4) {
        is_live = true;
    }

    xpath_node_set item_path_nodes;
    if (!is_live) {
        item_path_nodes = root_node.select_nodes("/root/channel/file/item");
    } else {
        xml_node stream_node = root_node.select_node("/root/channel/stream").node();
        interval = stream_node.attribute("interval").as_int(5);
        delay = stream_node.attribute("delay").as_int(45);
        item_path_nodes = root_node.select_nodes("/root/channel/stream/item");
    }
    if (item_path_nodes.empty()) {
        UTIL_LOG_ERROR(NULL, "[%s] not found file items\n", __func__);
        parse_success = false;
        return;
    }

    for (int i = 0; i < item_path_nodes.size(); ++i) {
        xml_node node = item_path_nodes[i].node();
        pptv::FileItem* item = NULL;
        if (is_live) {
            int basetime = 0;
            std::string basetime_from_url = pptv_url.param("basetime");
            // url中没有basetime时，设置为当前时间-delay，必须是5的倍数
            if (basetime_from_url.empty()) {
                basetime = time(NULL);
                basetime -= delay;
                basetime = (basetime / 5) * 5;
                UTIL_LOG_WARNING(NULL, "[%s] none basetime in url, use default %d", __func__, basetime);
            } else {
                basetime = boost::lexical_cast<int>(basetime_from_url);
            }
            item = new pptv::LiveFileItem(basetime, interval, delay, p2p_port);
        } else {
            item = new pptv::VodFileItem(p2p_port);
        }
        item->rid = node.attribute("rid").as_string("");
        item->bitrate = node.attribute("bitrate").as_int(-1);
        item->ft = node.attribute("ft").as_string("");
        item->width = node.attribute("width").as_int(-1);
        item->height = node.attribute("height").as_int(-1);
        item->file_size = node.attribute("filesize").as_int(-1);
        item->format = node.attribute("format").as_string("");
        item->pptv_url = pptv_url;

        add_file(item);
    }

    xpath_node_set dt_path_nodes = root_node.select_nodes("/root/dt");
    if (dt_path_nodes.empty()) {
        UTIL_LOG_ERROR(NULL, "[%s] not found dt\n", __func__);
        parse_success = false;
        return;
    }
    for (int i = 0; i < dt_path_nodes.size(); ++i) {
        xml_node node = dt_path_nodes[i].node();
        std::string ft = node.attribute("ft").as_string("");
        std::string rid = node.attribute("rid").as_string("");
        Dt dt;
        dt.sh = node.child("sh").text().as_string("");
        dt.id = node.child("id").text().as_string("");
        dt.bwt = node.child("bwt").text().as_int(1);
        dt.key = node.child("key").text().as_string("");
        dt.bh = node.child("bh").text().as_string("");
        dt.st = node.child("st").text().as_string("");
        dt.iv = node.child("iv").text().as_string("");
        dt.flag = node.child("flag").text().as_int(0);
        add_dt(dt, ft, rid);
    }

    if (is_live) {
        UTIL_LOG_INFO(NULL, "[%s] live, not parse drag", __func__);
        return;
    }
    xpath_node_set drag_path_nodes = root_node.select_nodes("/root/drag");
    if (drag_path_nodes.empty()) {
        UTIL_LOG_ERROR(NULL, "[%s] not found drag\n", __func__);
        parse_success = false;
        return;
    }

    for (int i = 0; i < drag_path_nodes.size(); ++i) {
        xml_node node = drag_path_nodes[i].node();
        std::string drag_ft = node.attribute("ft").as_string("0");
        std::string drag_rid = node.attribute("rid").as_string("");
        xpath_node_set sgm_path_nodes = node.select_nodes("sgm");
        UTIL_LOG_INFO(NULL, "[%s] drag_ft %s, drag_rid %s\n", __func__, drag_ft.c_str(), drag_rid.c_str());
        if (sgm_path_nodes.empty()) {
            UTIL_LOG_ERROR(NULL, "[%s] not found segments, ft %s, rid %s\n", __func__, drag_ft.c_str(),
                drag_rid.c_str());
            parse_success = false;
            continue;
        }
        for (size_t smg_index = 0; smg_index < sgm_path_nodes.size(); ++smg_index) {
            xml_node sgm_node = sgm_path_nodes[smg_index].node();
            Segment segment;
            segment.no = sgm_node.attribute("no").as_string("");
            segment.hl = sgm_node.attribute("hl").as_int();
            segment.duration_s = sgm_node.attribute("dur").as_float(0.0);
            segment.duration_ms = segment.duration_s * 1000;
            segment.fs = sgm_node.attribute("fs").as_int(0);
            segment.of = sgm_node.attribute("of").as_int(0);
            segment.rid = sgm_node.attribute("rid").as_string("");
            add_segment(segment, drag_ft, drag_rid);
        }
    }
}

PlayXml::~PlayXml()
{
    UTIL_LOG_INFO(NULL, "[%s]", __func__);
    while (!files.empty()) {
        FileItem* item = files.back();
        delete item;
        files.pop_back();
    }
}

void PlayXml::add_file(FileItem* file_item)
{
    files.push_back(file_item);
}

int PlayXml::get_duration_ms()
{
    if (is_live) {
        return -1;
    }
    return duration_ms;
}

void PlayXml::add_dt(Dt dt, std::string ft, std::string rid)
{
    dt.k_decrypted = create_dt_key(dt);
    // rid不为空，根据rid选择
    // rid为空、ft不为空，根据ft选择
    // rid和ft都为空，所有的file使用同一个dt
    if (!rid.empty()) {
        for (int i = 0; i < files.size(); ++i) {
            if (files[i]->rid == rid) {
                files[i]->dt = dt;
                UTIL_LOG_INFO(NULL, "[%s] set dt by rid %s", __func__, rid.c_str());
                return;
            }
        }
    }
    UTIL_LOG_WARNING(NULL, "[%s] rid empty or invalid, rid %s", __func__, rid.c_str());
    if (!ft.empty()) {
        for (int i = 0; i < files.size(); ++i) {
            if (files[i]->ft == ft) {
                files[i]->dt = dt;
                UTIL_LOG_INFO(NULL, "[%s] set dt by ft %s", __func__, ft.c_str());
                return;
            }
        }
    }
    UTIL_LOG_WARNING(NULL, "[%s] ft empty or invalid, ft %s", __func__, ft.c_str());
    for (int i = 0; i < files.size(); ++i) {
        UTIL_LOG_INFO(NULL, "[%s] set dt to rid %s", __func__, files[i]->rid.c_str());
        files[i]->dt = dt;
    }
}

std::string PlayXml::create_dt_key(Dt dt)
{
    std::string key;

    if (dt.iv.empty()) {
        UTIL_LOG_INFO(NULL, "not need anti_hotlinking_decrypt");
        key = dt.key;
    } else {
        int length = dt.key.length() + 1;
        unsigned char* k_hex_decrypt;
        k_hex_decrypt = new unsigned char[length];
        memset(k_hex_decrypt, 0, length);
        unsigned int k_hex_decrypt_len;
        k_hex_decrypt_len = dt.key.length();
        UTIL_LOG_INFO(NULL, "[%s] %s, %s, %s, %s, %s, %s,", __func__, dt.sh.c_str(), dt.st.c_str(), dt.id.c_str(), dt.bh.c_str(), dt.iv.c_str(), dt.key.c_str());
        // 默认appkey是android的
        if (anti_hotlinking_decrypt(dt.sh.c_str(), dt.st.c_str(),
                dt.id.c_str(), dt.bh.c_str(), dt.iv.c_str(), NULL, dt.key.c_str(),
                dt.flag, k_hex_decrypt, k_hex_decrypt_len)
            == 0) {
            if (k_hex_decrypt_len > 0) {
                for (size_t i = 0; i < k_hex_decrypt_len; i++) {
                    key += k_hex_decrypt[i];
                }
            }
        } else {
            key = dt.key;
        }
        delete[] k_hex_decrypt;
        k_hex_decrypt = NULL;
    }

    return key;
}

void PlayXml::add_segment(Segment segment, std::string ft, std::string rid)
{
    bool selected = false;
    for (int i = 0; i < files.size(); ++i) {
        if (!ft.empty() && !rid.empty() && files[i]->ft == ft && files[i]->rid == rid) {
            selected = true;
        } else if (!rid.empty() && files[i]->rid == rid) {
            selected = true;
        } else if (!ft.empty() && files[i]->ft == ft) {
            selected = true;
        }
        if (selected) {
            files[i]->add_segment(segment);
            break;
        }
    }
    if(!selected) {
        UTIL_LOG_WARNING(NULL, "[%s] not found file for segment, ft %s, rid %s", __func__, ft.c_str(), rid.c_str());
    }
}

FileItem* PlayXml::select_file(std::string ft, std::string rid)
{
    UTIL_LOG_INFO(NULL, "[%s] ft %s, rid %s, files.size %d", __func__, ft.c_str(), rid.c_str(), files.size());
    current_ft = ft;
    current_rid = rid;
    for (int i = 0; i < files.size(); ++i) {
        if (files[i]->ft == ft && files[i]->rid == rid) {
            return files[i];
        }
    }
    for (int i = 0; i < files.size(); ++i) {
        if (files[i]->ft == ft) {
            return files[i];
        }
    }
    return NULL;
}

bool PlayXml::vaild()
{
    return parse_success;
}

bool PlayXml::live()
{
    return is_live;
}
}