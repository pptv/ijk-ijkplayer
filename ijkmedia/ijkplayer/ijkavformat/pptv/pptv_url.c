#include <assert.h>
#include "libavformat/avformat.h"
#include "libavformat/url.h"
#include "libavutil/avstring.h"
#include "libavutil/log.h"
#include "libavutil/opt.h"

#include "libavutil/application.h"

typedef struct PptvUrlContext {
    AVClass        *class;
    URLContext     *inner;

    /* options */
    char           *http_hook;
    // int64_t         app_ctx_intptr;
} PptvUrlContext;

static int pptv_url_open(URLContext *h, const char *arg, int flags, AVDictionary **options)
{
    av_log(NULL, AV_LOG_INFO, "[pptv_url_open]\n");
    PptvUrlContext *c = h->priv_data;
    AVAppIOControl io_control = {0};
    // AVApplicationContext *app_ctx = (AVApplicationContext *)(intptr_t)c->app_ctx_intptr;
    int ret = -1;

    // av_strstart(arg, "pptv:", &arg);

    if (!arg || !*arg)
        return AVERROR_EXTERNAL;

    // segment_index = (int)strtol(arg, NULL, 0);
    // io_control.size = sizeof(io_control);
    // io_control.segment_index = segment_index;
    strlcpy(io_control.url,    arg,    sizeof(io_control.url));

    // if (app_ctx && io_control.segment_index < 0) {
    //     ret = AVERROR_EXTERNAL;
    //     goto fail;
    // }
    // ret = av_application_on_io_control(app_ctx, AVAPP_CTRL_WILL_CONCAT_SEGMENT_OPEN, &io_control);
    // if (ret || !io_control.url[0]) {
    //     ret = AVERROR_EXIT;
    //     goto fail;
    // }

    // av_dict_set_int(options, "ijkapplication", c->app_ctx_intptr, 0);
    // av_dict_set_int(options, "ijkinject-segment-index", segment_index, 0);

    // ret = ffurl_open_whitelist(&c->inner,
    //                            io_control.url,
    //                            flags,
    //                            &h->interrupt_callback,
    //                            options,
    //                            h->protocol_whitelist,
    //                            h->protocol_blacklist,
    //                            h);
    // if (ret)
    //     goto fail;

    return 0;
}

static int pptv_url_close(URLContext *h)
{
    PptvUrlContext *c = h->priv_data;

    return 0;
}

static int pptv_url_read(URLContext *h, unsigned char *buf, int size)
{
    static int retured = 0;
    if (!retured)
    {
        PptvUrlContext *c = h->priv_data;
        av_log(NULL, AV_LOG_INFO, "[pptv_url_read] size %d\n", size);
        strlcpy(buf, "pptv", size);
        retured = 1;
        return 5;
    }
    
    return AVERROR(EOF);
}

static int64_t pptv_url_seek(URLContext *h, int64_t pos, int whence)
{
    PptvUrlContext *c = h->priv_data;

    return 0;
}

// #define OFFSET(x) offsetof(Context, x)
// #define D AV_OPT_FLAG_DECODING_PARAM

static const AVOption options[] = {
    // { "ijkapplication", "AVApplicationContext", OFFSET(app_ctx_intptr), AV_OPT_TYPE_INT64, { .i64 = 0 }, INT64_MIN, INT64_MAX, .flags = D },
    { NULL }
};

// #undef D
// #undef OFFSET

static const AVClass pptv_url_context_class = {
    .class_name = "Pptv",
    .item_name  = av_default_item_name,
    .option     = options,
    .version    = LIBAVUTIL_VERSION_INT,
};

URLProtocol ijkimp_ff_pptv_protocol = {
    .name                = "pptv",
    .url_open2           = pptv_url_open,
    .url_read            = pptv_url_read,
    .url_seek            = pptv_url_seek,
    .url_close           = pptv_url_close,
    .priv_data_size      = sizeof(PptvUrlContext),
    .priv_data_class     = &pptv_url_context_class,
};
