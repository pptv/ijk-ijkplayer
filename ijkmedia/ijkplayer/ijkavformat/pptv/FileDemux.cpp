//
// Created by sizhenzhu on 2018/9/12.
//

#include "FileDemux.h"
#include <framework/string/Url.h>

#include "util.h"

namespace pptv {

const int FileDemux::NEXT_SEGMENT_OPEN_MAX_RETRY = 100;
const int FileDemux::SEGMENT_OPEN_MAX_RETRY_VOD = 5;
const int FileDemux::SEGMENT_OPEN_MAX_RETRY_LIVE = 2;

FileDemux::FileDemux(FileItem* item, AVFormatContext* context, FFPlayer *ffplayer)
    : file_item(item)
    , wrap_format_context(context)
    , segment_demux(NULL)
    , last_video_pts(0)
    , last_audio_pts(0)
    , audio_start_pts(0)
    , video_start_pts(0)
    , ffp(ffplayer)
{
    if (file_item->is_live) {
        segment_max_open_times = SEGMENT_OPEN_MAX_RETRY_LIVE;
    } else {
        segment_max_open_times = SEGMENT_OPEN_MAX_RETRY_VOD;
    }
}

FileDemux::~FileDemux()
{
    if (segment_demux) {
        delete segment_demux;
        segment_demux = NULL;
    }
    file_item = NULL;
}

bool FileDemux::open()
{
    bool segment_open_result = open_next_segment();
    if (segment_open_result && file_item->is_live) {
        AVFormatContext* segment_format_context = segment_demux->get_format_context();
        audio_start_pts = segment_format_context->streams[AUDIO_STREAM_INDEX]->start_time;
        video_start_pts = segment_format_context->streams[VIDEO_STREAM_INDEX]->start_time;
        UTIL_LOG_INFO(NULL, "[%s] audio_start_pts %lld, video_start_pts %lld", __func__, audio_start_pts, video_start_pts);
    }

    return segment_open_result;
}

bool FileDemux::open_next_segment()
{
    bool open_ret = false;
    int open_times = 0;
    while(file_item->have_next_segment() && open_times < NEXT_SEGMENT_OPEN_MAX_RETRY) {
        if (ff_check_interrupt(&wrap_format_context->interrupt_callback)) {
            UTIL_LOG_INFO(NULL, "[%s] interrupted", __func__);
            break;
        }
        current_segment = file_item->next_segment();
        open_ret = open_segment();
        if (open_ret) {
            break;
        }
        open_times++;
    }
    UTIL_LOG_INFO(NULL, "[%s] open_ret %d, open_times %d", __func__, open_ret, open_times);
    return open_ret;
}

bool FileDemux::open_segment()
{
    if (segment_demux) {
        delete segment_demux;
        segment_demux = NULL;
    }
    UTIL_LOG_INFO(NULL, "[%s] url %s, start_time_ms %lld, is_live %d\n", __func__, current_segment.url.c_str(),
        current_segment.start_time_ms, current_segment.is_live);

    bool open_ret = false;
    for (int i = 1; i <= segment_max_open_times; ++i) {
        if (ff_check_interrupt(&wrap_format_context->interrupt_callback)) {
            UTIL_LOG_INFO(NULL, "[%s] interrupted", __func__);
            break;
        }
        segment_demux = new SegmentDemux(current_segment, wrap_format_context, ffp);
        open_ret = segment_demux->open();
        if (open_ret) {
            break;
        } else {
            UTIL_LOG_WARNING(NULL, "[%s] segment open failed, retry times %d", __func__, i);
            delete segment_demux;
            segment_demux = NULL;
        }
    }

    UTIL_LOG_INFO(NULL, "[%s] segment_demux open result %d\n", __func__, open_ret);
    if (open_ret) {
        ffp_notify_msg4(ffp, FFP_MSG_PPTV_SEGMENT_CHANGE, 0, 0, (void *)current_segment.rid.c_str(), strlen(current_segment.rid.c_str())+1);
    }
    return open_ret;
}

int FileDemux::read_packet(AVPacket* pkt)
{
    if (!segment_demux) {
        UTIL_LOG_WARNING(NULL, "[%s] segment_demux is NULL", __func__);
        return AVERROR_EXIT;
    }
    int ret = segment_demux->read_packet(pkt);
    if (ret == AVERROR_EOF) {
        if (!file_item->have_next_segment()) {
            UTIL_LOG_INFO(NULL, "[%s] reach end\n", __func__);
            return ret;
        }
        if (!open_next_segment()) {
            UTIL_LOG_ERROR(NULL, "[%s] open_next_segment failed\n", __func__);
            return AVERROR_EXIT;
        }
        ret = segment_demux->read_packet(pkt);
    }
    if (ret == 0) {
        if (pkt->stream_index == VIDEO_STREAM_INDEX) {
            last_video_pts = pkt->pts;
        } else if (pkt->stream_index == AUDIO_STREAM_INDEX) {
            last_audio_pts = pkt->pts;
        }
    }
    return ret;
}

int FileDemux::seek(int stream_index, int64_t time_ms, int flags)
{
    // if (file_item.is_live)
    // {
    //     UTIL_LOG_WARNING(NULL, "[%s] live not support seek now", __func__);
    //     return -1;
    // }
    if (stream_index != -1) {
        UTIL_LOG_ERROR(NULL, "[%s] invalid stream index %d\n", __func__, stream_index);
        return -1;
    }
    if (time_ms < 0) {
        UTIL_LOG_WARNING(NULL, "[%s] adjust seek time %lld to 0", __func__, time_ms);
        time_ms = 0;
    }
    if (!file_item->is_live && time_ms > file_item->duration_ms) {
        UTIL_LOG_WARNING(NULL, "[%s] adjust seek time %lld to %lld", __func__, time_ms, file_item->duration_ms);
        time_ms = file_item->duration_ms;
    }

    Segment target_segment = file_item->find_segment(time_ms);
    UTIL_LOG_INFO(NULL, "[%s] target_segment %s", __func__, target_segment.no.c_str());
    if (target_segment.no != current_segment.no) {
        UTIL_LOG_INFO(NULL, "[%s] current_segment.no %s, target_segment.no %s",
            __func__, current_segment.no.c_str(), target_segment.no.c_str());
        current_segment = target_segment;
        file_item->current_index = current_segment.index;
        if (!open_segment()) {
            UTIL_LOG_ERROR(NULL, "[%s] open_segment failed\n", __func__);
            return -1;
        }
    }
    if (!segment_demux) {
        UTIL_LOG_WARNING(NULL, "[%s] segment_demux is NULL", __func__);
        return -1;
    }
    int64_t seek_time_in_segment = time_ms - current_segment.start_time_ms;
    UTIL_LOG_INFO(NULL, "[%s] seek to %lld int segment %d\n", __func__, seek_time_in_segment, current_segment.index);
    int ret = segment_demux->seek(stream_index, seek_time_in_segment, flags);
    UTIL_LOG_INFO(NULL, "[%s] segment_demux seek ret %d\n", __func__, ret);

    // 切换画质的时候如果seek失败，打开下一个segment
    if ((ret < 0) && (flags & AVSEEK_FLAG_FOR_SWITCH)) {
        UTIL_LOG_WARNING(NULL, "[%s] seek for switch failed, %d, open next segment", __func__, ret);
        return open_next_segment();
    } else {
        return ret;
    }
}

AVFormatContext* FileDemux::get_format_context()
{
    return segment_demux->get_format_context();
}

uint64_t FileDemux::last_video_time_ms()
{
    AVRational timebase = wrap_format_context->streams[VIDEO_STREAM_INDEX]->time_base;
    double time_ms = (last_video_pts - video_start_pts) * av_q2d(timebase) * 1000;
    return time_ms;
}

uint64_t FileDemux::last_audio_time_ms()
{
    AVRational timebase = wrap_format_context->streams[AUDIO_STREAM_INDEX]->time_base;;
    double time_ms = (last_audio_pts - audio_start_pts) * av_q2d(timebase) * 1000;
    return time_ms;
}

int FileDemux::current_segment_index()
{
    return current_segment.index;
}
}