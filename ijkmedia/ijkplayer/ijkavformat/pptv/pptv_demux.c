#include "libavformat/avformat.h"
#include "libavformat/url.h"
#include "libavutil/avstring.h"
#include "libavutil/opt.h"
#include <pthread.h>

#include "ijkplayer/ijkavutil/opt.h"

#include "ff_ffplay.h"
#include "../ijkavformat.h"
#include "libavutil/application.h"

#include "MultiFileDemux.h"

#include "util.h"

typedef struct
{
    AVClass* class;
    void* file_demux;

    AVAppIOControl io_control;
    int discontinuity;
    int error;
    FFPlayer *ffp;
    SwitchSourceContext* switch_context;

    /* options */
    char* play_xml;
    int64_t pptv_context_intptr;
    int p2p_port;
} Context;

static int pptv_demux_probe(AVProbeData* probe)
{
    UTIL_LOG_INFO(NULL, "[%s]\n", __func__);
    if (av_strstart(probe->filename, "pptv", NULL)) {
        UTIL_LOG_INFO(NULL, "[%s] %s\n", __func__, probe->filename);
        return AVPROBE_SCORE_MAX;
    }

    return 0;
}

static int pptv_demux_read_close(AVFormatContext* avf)
{
    Context* context = avf->priv_data;
    if (context->file_demux) {
        delete_multi_file_demux(context->file_demux);
        context->file_demux = NULL;
    }
    return 0;
}

// FIXME: install libavformat/internal.h
int ff_alloc_extradata(AVCodecParameters* par, int size);

static int copy_stream_props(AVStream* st, AVStream* source_st)
{
    int ret;

    st->codecpar->codec_id = source_st->codecpar->codec_id;
    if (st->codecpar->extradata_size < source_st->codecpar->extradata_size) {
        if (st->codecpar->extradata) {
            av_freep(&st->codecpar->extradata);
            st->codecpar->extradata_size = 0;
        }
        ret = ff_alloc_extradata(st->codecpar, source_st->codecpar->extradata_size);
        if (ret < 0) {
            UTIL_LOG_ERROR(NULL, "[%s] ff_alloc_extradata failed\n", __func__);
            return ret;
        }
    }
    memcpy(st->codecpar->extradata, source_st->codecpar->extradata, source_st->codecpar->extradata_size);

    if ((ret = avcodec_parameters_copy(st->codecpar, source_st->codecpar)) < 0) {
        UTIL_LOG_ERROR(NULL, "[%s] avcodec_parameters_copy failed\n", __func__);
        return ret;
    }
    st->r_frame_rate = source_st->r_frame_rate;
    st->avg_frame_rate = source_st->avg_frame_rate;
    st->time_base = source_st->time_base;
    st->sample_aspect_ratio = source_st->sample_aspect_ratio;
    st->start_time = source_st->start_time;

    av_dict_copy(&st->metadata, source_st->metadata, 0);
    return ret;
}

static int add_stream_to_format(AVFormatContext* context, AVStream* orig_stream)
{
    int ret = 0;
    AVStream* new_stream = avformat_new_stream(context, NULL);
    if (!new_stream) {
        UTIL_LOG_ERROR(NULL, "[%s] add_stream_to_format failed\n", __func__);
        return AVERROR(ENOMEM);
    }
    ret = copy_stream_props(new_stream, orig_stream);
    if (ret < 0) {
        UTIL_LOG_ERROR(NULL, "[%s] copy_stream_props failed\n", __func__);
    }
    return ret;
}

static int open_file_demux(AVFormatContext* avf)
{
    Context* context = avf->priv_data;
    AVFormatContext* new_avf = NULL;
    int ret = 0;

    context->file_demux = new_multi_file_demux(avf->filename, context->play_xml, avf, context->p2p_port, context->ffp);
    if (multi_file_demux_open(context->file_demux) != 0) {
        UTIL_LOG_ERROR(NULL, "[%s] file_demux_open failed\n", __func__);
        ret = AVERROR_INVALIDDATA;
        goto fail;
    }
    UTIL_LOG_INFO(NULL, "[%s] duration %lld", __func__, avf->duration);

    new_avf = multi_file_demux_get_format_context(context->file_demux);
    UTIL_LOG_INFO(NULL, "[%s] new_avf nb_streams %d\n", __func__, new_avf->nb_streams);

    int video_stream_index = find_video_stream_index(new_avf);
    int audio_stream_index = find_audio_stream_index(new_avf);
    if (video_stream_index < 0 || audio_stream_index < 0) {
        UTIL_LOG_WARNING(NULL, "[%s] video_stream_index %d, audio_stream_index %d", __func__, video_stream_index,
            audio_stream_index);
        ret =  AVERROR_EXIT;
        goto fail;
    }
    if ((ret = add_stream_to_format(avf, new_avf->streams[video_stream_index])) < 0) {
        UTIL_LOG_ERROR(NULL, "[%s] add video stream failed\n", __func__);
        goto fail;
    }
    if ((ret = add_stream_to_format(avf, new_avf->streams[audio_stream_index])) < 0) {
        UTIL_LOG_ERROR(NULL, "[%s] add audio stream failed\n", __func__);
        goto fail;
    }

    return ret;
fail:
    delete_multi_file_demux(context->file_demux);
    context->file_demux = NULL;
    return ret;
}

static int pptv_demux_read_header(AVFormatContext* avf, AVDictionary** options)
{
    Context* context = avf->priv_data;
    int ret = -1;

    UTIL_LOG_INFO(NULL, "[%s] play_xml %s\n", __func__, context->play_xml);
    context->io_control.size = sizeof(context->io_control);
    context->ffp = (FFPlayer*)(intptr_t)context->pptv_context_intptr;
    context->switch_context = &context->ffp->is->switch_source_context;
    UTIL_LOG_INFO(NULL, "[%s] ffp %p, switch_context %p\n", __func__, context->ffp, context->switch_context);

    context->io_control.retry_counter = 0;
    ret = open_file_demux(avf);
    UTIL_LOG_INFO(NULL, "[%s] open_file_demux ret %d\n", __func__, ret);
    return ret;
}

static int pptv_demux_read_packet(AVFormatContext* avf, AVPacket* pkt)
{
    Context* context = avf->priv_data;
    SwitchSourceContext* switch_context = context->switch_context;
    int ret = -1;

    if (context->error) {
        return context->error;
    }
    if (!context->file_demux) {
        UTIL_LOG_WARNING(NULL, "[%s] file_demux is NULL", __func__);
        return AVERROR_EXIT;
    }
    if (switch_context) {
        if (pthread_mutex_trylock(&switch_context->lock) == 0) {
            if (switch_context->have_switch) {
                UTIL_LOG_INFO(NULL, "[%s] got switch %s", __func__, switch_context->url);
                multi_file_demux_switch(context->file_demux, switch_context->url);
                switch_context->have_switch = 0;
                av_free(switch_context->url);
                switch_context->url = NULL;
            }
            pthread_mutex_unlock(&switch_context->lock);
        } else {
            UTIL_LOG_WARNING(NULL, "[%s] lock failed", __func__);
        }
    } else {
        UTIL_LOG_WARNING(NULL, "[%s] switch_context is NULL", __func__);
    }

    if (context->file_demux) {
        ret = multi_file_demux_read_packet(context->file_demux, pkt);
    }

    return ret;
}

static int pptv_demux_read_seek(AVFormatContext* s, int stream_index, int64_t ts, int flags)
{
    Context* context = s->priv_data;
    return multi_file_demux_seek(context->file_demux, stream_index, ts, flags);
}

#define OFFSET(x) offsetof(Context, x)
#define D AV_OPT_FLAG_DECODING_PARAM

static const AVOption options[] = {
    { "pptv-play-xml", "pptv play xml",
        OFFSET(play_xml), AV_OPT_TYPE_STRING, { .str = NULL }, 0, 0, D },
    { "pptv-context", "pptv context",
        OFFSET(pptv_context_intptr), AV_OPT_TYPE_INT64, { .i64 = 0 }, INT64_MIN, INT64_MAX, .flags = D },
    { "pptv-p2p-port", "pptv p2p port",
        OFFSET(p2p_port), AV_OPT_TYPE_INT, 0, 0, INT_MAX, .flags = D },
    { NULL }
};

#undef D
#undef OFFSET

static const AVClass pptvdemux_class = {
    .class_name = "Pptv demux",
    .item_name = av_default_item_name,
    .option = options,
    .version = LIBAVUTIL_VERSION_INT,
};

AVInputFormat ijkff_pptvdemux_demuxer = {
    .name = "pptvdemux",
    .long_name = "Pptv demux",
    .flags = AVFMT_NOFILE | AVFMT_TS_DISCONT,
    .priv_data_size = sizeof(Context),
    .read_probe = pptv_demux_probe,
    .read_header2 = pptv_demux_read_header,
    .read_packet = pptv_demux_read_packet,
    .read_seek = pptv_demux_read_seek,
    .read_close = pptv_demux_read_close,
    .priv_class = &pptvdemux_class,
};
