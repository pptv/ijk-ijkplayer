//
// Created by sizhenzhu on 2018/9/12.
//

#ifndef JUST_FILEDEMUX_H
#define JUST_FILEDEMUX_H

#ifdef __cplusplus
#define __STDC_CONSTANT_MACROS
#ifdef _STDINT_H
#undef _STDINT_H
#endif

#include <stdint.h>

#endif

#ifdef __cplusplus
extern "C" {
#endif
#include "ff_ffplay.h"
#include "libavformat/avformat.h"
#include "libavformat/url.h"
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

#include "PlayXml.h"
#include "SegmentDemux.h"
#include <string>

namespace pptv {

class FileDemux {
public:
    FileDemux(FileItem* item, AVFormatContext* context, FFPlayer *ffp);

    ~FileDemux();

    bool open();
    int read_packet(AVPacket* pkt);
    AVFormatContext* get_format_context();
    int seek(int stream_index, int64_t time_ms, int flags);
    uint64_t last_video_time_ms();
    uint64_t last_audio_time_ms();
    int current_segment_index();

private:
    // 不需要释放
    AVFormatContext* wrap_format_context;
    std::string ft;
    std::string rid;
    SegmentDemux* segment_demux;
    int64_t played_segments_duration;
    Segment current_segment;
    FileItem* file_item;
    int64_t last_video_pts;
    int64_t last_audio_pts;
    int64_t audio_start_pts;
    int64_t video_start_pts;
    FFPlayer *ffp;
    int segment_max_open_times;

    bool open_next_segment();
    bool open_segment();

    static const int SEGMENT_OPEN_MAX_RETRY_VOD;
    static const int SEGMENT_OPEN_MAX_RETRY_LIVE;
    static const int NEXT_SEGMENT_OPEN_MAX_RETRY;
};

}
#endif

#endif //JUST_FILEDEMUX_H
