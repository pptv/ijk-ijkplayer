//
// Created by sizhenzhu on 2018/8/23.
//

#ifndef PLAYXML_PLAYXML_H
#define PLAYXML_PLAYXML_H

#include <string>
#include <vector>

#include <framework/string/Url.h>

namespace pptv {

struct Dt {
    std::string sh;
    std::string st;
    std::string id;
    int bwt;
    std::string bh;
    std::string key;
    std::string iv;
    int flag;
    std::string k_decrypted;
};

struct Segment {
    std::string no;
    int hl;
    float duration_s;
    int duration_ms;
    int64_t start_time_ms;
    int fs;
    int of;
    std::string rid;
    std::string url;
    int index;
    bool is_live;
    int start_offset;
};

struct FileItem {
    std::string rid;
    int bitrate;
    std::string ft;
    int file_size;
    int width;
    int height;
    std::string format;
    Dt dt;
    std::vector<Segment> segments;
    int current_index;
    int64_t duration_ms;
    bool is_live;
    framework::string::Url pptv_url;
    int p2p_port;

public:
    FileItem(int port = 0);
    virtual ~FileItem();
    virtual Segment next_segment()=0;
    virtual Segment find_segment(int64_t target_time_ms)=0;
    void add_segment(Segment segment);
    virtual bool have_next_segment()=0;
    void set_current_index(int index);
    framework::string::Url p2p_prepare(std::string cdn_url);
};

struct LiveFileItem : FileItem {
    int basetime;
    int interval;
    int delay;
    size_t seq;

public:
    LiveFileItem(int base, int interval_value, int delay_value, int port);
    virtual ~LiveFileItem();
    virtual Segment next_segment();
    virtual Segment find_segment(int64_t target_time_ms);
    virtual bool have_next_segment();

private:
    Segment make_live_segment(int index);
    std::string segment_url(Segment segment, int block);
    framework::string::Url cdn_segment_url(pptv::Segment segment, int block);
    framework::string::Url p2p_segment_url(pptv::Segment segment, int block);
};

struct VodFileItem : FileItem {
public:
    VodFileItem(int port);
    virtual ~VodFileItem();
    virtual Segment next_segment();
    virtual Segment find_segment(int64_t target_time_ms);
    virtual bool have_next_segment();

private:
    std::string segment_url(Segment segment);
    framework::string::Url cdn_segment_url(pptv::Segment segment);
    framework::string::Url p2p_segment_url(pptv::Segment segment);
};

class PlayXml {
    std::vector<FileItem*> files;
    int video_type;
    bool is_live;
    bool parse_success;
    framework::string::Url pptv_url;
    int duration_ms;
    std::string current_ft;
    std::string current_rid;
    int p2p_port;

public:
    PlayXml(const char* xml, const char* url, int port);
    ~PlayXml();
    void add_file(FileItem* file_item);
    void add_dt(Dt dt, std::string ft, std::string rid);
    void add_segment(Segment segment, std::string ft, std::string rid);

    FileItem* select_file(std::string ft, std::string rid);
    bool vaild();
    int get_duration_ms();
    bool live();

private:
    std::string create_dt_key(Dt dt);
};
}

#endif //PLAYXML_PLAYXML_H
