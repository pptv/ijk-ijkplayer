//
// Created by sizhenzhu on 2018/9/12.
//

#ifndef JUST_MULTI_FILEDEMUX_H
#define JUST_MULTI_FILEDEMUX_H

#ifdef __cplusplus
#define __STDC_CONSTANT_MACROS
#ifdef _STDINT_H
#undef _STDINT_H
#endif

#include <stdint.h>

#endif

#ifdef __cplusplus
extern "C" {
#endif
#include "libavformat/avformat.h"
#include "ff_ffplay.h"
#include "ijkavformat/ijkavformat.h"
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

#include "FileDemux.h"
#include "PlayXml.h"
#include <framework/string/Url.h>
#include <string>

namespace pptv {

struct SwitchInfo {
    std::string ft;
    std::string rid;
    framework::string::Url url;
    FileDemux* demux;
    AVPacket* pkt;
    uint64_t switch_time_ms;

    SwitchInfo();
    ~SwitchInfo();
};

class MultiFileDemux {
public:
    MultiFileDemux(char* url, char* play_xml_content, AVFormatContext* context, int port, FFPlayer *ffplayer);

    ~MultiFileDemux();

    bool open();
    int read_packet(AVPacket* pkt);
    AVFormatContext* get_format_context();
    int seek(int stream_index, int64_t time_ms, int flags);
    bool switch_stream(char* url);
    int seek_for_switch(uint64_t target_time_ms, SwitchInfo *switching_info);

private:
    // 不需要释放
    AVFormatContext* wrap_format_context;
    PlayXml play_xml;
    std::string ft;
    std::string rid;
    FileDemux* file_demux;
    SwitchInfo* switching_info;
    SwitchInfo* next_switch_info;
    FFPlayer *ffp;
    int cast_to_int(std::string val);
};

}
#endif

#ifdef __cplusplus
extern "C" {
#endif

void* new_multi_file_demux(char* url, char* play_xml_content, AVFormatContext* context, int port, FFPlayer *ffp);
void delete_multi_file_demux(void* demux);
int multi_file_demux_open(void* demux);
int multi_file_demux_read_packet(void* demux, AVPacket* pkt);
AVFormatContext* multi_file_demux_get_format_context(void* demux);
int multi_file_demux_seek(void* demux, int stream_index, int64_t ts, int flags);
int multi_file_demux_switch(void* demux, char* new_url);

#ifdef __cplusplus
}
#endif

#endif //JUST_MULTI_FILEDEMUX_H
