#ifndef IJK_SEGMENTDEMUXER_H
#define IJK_SEGMENTDEMUXER_H

#ifdef __cplusplus
#define __STDC_CONSTANT_MACROS
#ifdef _STDINT_H
#undef _STDINT_H
#endif
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif
#include "ff_ffplay.h"
#include "libavformat/avformat.h"
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
#include "PlayXml.h"
#include <string>

// 分段解封装

namespace pptv {

class SegmentDemux {

public:
    SegmentDemux(Segment current_segment, AVFormatContext* prev_context, FFPlayer *ffplayer);
    ~SegmentDemux();
    int read_packet(AVPacket* pkt);
    AVFormatContext* get_format_context();
    bool open();
    int seek(int stream_index, int64_t time_ms, int flags);
    AVRational video_timebase();
    AVRational audio_timebase();

private:
    Segment segment;
    std::string url;
    AVFormatContext* format_context;
    AVFormatContext* wrap_format_context;
    // in stream timebase
    int64_t video_offset;
    int64_t audio_offset;
    // in millsecond
    int64_t time_offset_ms;
    AVDictionary* open_opts;
    int video_stream_index;
    int audio_stream_index;
    bool video_pkt_flag_set;
    bool audio_pkt_flag_set;
    FFPlayer *ffp;

    int64_t cal_offset(int stream_index);
    bool open_input();
    int read_vod_packet(AVPacket* pkt);
    int read_live_packet(AVPacket* pkt);
    int read_packet_from_format_context(AVPacket* pkt);
    void adjust_pts_and_stream_index(AVPacket* pkt);
    void add_side_data(AVPacket* pkt);
    void set_extradata_sent(int stream_index, bool sent);
};

}
#endif

#endif //IJK_SEGMENTDEMUXER_H
