#ifndef ANTI_HOTLINKING_C_H
#define ANTI_HOTLINKING_C_H
#include "anti-hotlinking.h"

#ifdef ANTI_HOTLINKING_C_STATIC
#   define ANTI_HOTLINKING_C_API
#else
#   if defined(_MSC_VER)
#       ifdef ANTI_HOTLINKING_C_EXPORT
#           define ANTI_HOTLINKING_C_API __declspec(dllexport)
#       else
#           define ANTI_HOTLINKING_C_API __declspec(dllimport)
#       endif
#   else
#       define ANTI_HOTLINKING_C_API __attribute__ ((visibility ("default")))
#   endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

//初始化
ANTI_HOTLINKING_C_API int anti_hotlinking_initialize_client();

//反初始化
ANTI_HOTLINKING_C_API int anti_hotlinking_uninitialize_client();

//取得APP类型
ANTI_HOTLINKING_C_API const char* anti_hotlinking_type();

//取得库版本号
ANTI_HOTLINKING_C_API int anti_hotlinking_version();

//产生签名
ANTI_HOTLINKING_C_API int anti_hotlinking_generate_signature(
    const char    *type,
    unsigned char random_hex[ANTI_HOTLINKING_CIPHER_BLOCK_HEX_SIZE],
    unsigned int  &random_hex_len,
    unsigned char signature_hex[ANTI_HOTLINKING_SIGNATURE_HEX_LEN],
    unsigned int  &signature_hex_len);

//解密
ANTI_HOTLINKING_C_API int anti_hotlinking_decrypt(
    const char *sh,
    const char *st,
    const char *id,
    const char *bh,
    const char *iv_hex,
    const char *hmac_hex,
    const char *ke_hex,
    int flag,
    unsigned char k_hex[ANTI_HOTLINKING_K_HEX_LEN],
    unsigned int  &k_hex_len);

//解密2
ANTI_HOTLINKING_C_API int anti_hotlinking_decrypt2(
    int type_id,        //0:pc 1:ios 2:andriod 3:flash 4:html5
    const char *sh,
    const char *st,
    const char *id,
    const char *bh,
    const char *iv_hex,
    const char *hmac_hex,
    const char *ke_hex,
    int flag,
    unsigned char k_hex[ANTI_HOTLINKING_K_HEX_LEN],
    unsigned int  &k_hex_len);

#ifdef __cplusplus
}
#endif

#endif

