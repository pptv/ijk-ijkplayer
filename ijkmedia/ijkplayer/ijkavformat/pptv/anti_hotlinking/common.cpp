#include "common.h"
#include "SL_Crypto_Hex.h"
#include "SL_ByteBuffer.h"
#include "SL_Crypto_SHA256.h"

int anti_hotlinking_generate_key(
    int flag,
    const char *sh,
    const char *st,
    const char *id,
    const char *bh,
    const char *iv_hex,
    const char *app_key,
    unsigned char key[ANTI_HOTLINKING_APP_KEY_LEN])
{
    SL_ByteBuffer key_buffer(256);
    if ( (flag & ANTI_HOTLINKING_FLAG_SH_MASK) && (NULL != sh) )
    {
        key_buffer.write(sh, strlen(sh));
    }
    if ( (flag & ANTI_HOTLINKING_FLAG_ST_MASK) && (NULL != st) )
    {
        key_buffer.write(st, strlen(st));
    }
    if ((flag & ANTI_HOTLINKING_FLAG_ID_MASK) && (NULL != id) )
    {
        key_buffer.write(id, strlen(id));
    }
    if ( (flag & ANTI_HOTLINKING_FLAG_BH_MASK) && (NULL != bh) )
    {
        key_buffer.write(bh, strlen(bh));
    }

    key_buffer.write(iv_hex, ANTI_HOTLINKING_CIPHER_BLOCK_HEX_SIZE);
    key_buffer.write(app_key, ANTI_HOTLINKING_APP_KEY_LEN);
    SL_Crypto_SHA256 hash;
    hash.final((const unsigned char *)key_buffer.data(), key_buffer.data_size(), key);

    return 0;
}

