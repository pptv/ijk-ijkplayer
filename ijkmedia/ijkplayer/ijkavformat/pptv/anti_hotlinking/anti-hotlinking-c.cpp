#include "SL_Crypto_Hex.h"
#include "SL_Crypto_CRTRand.h"
#include "SL_Crypto_SHA256.h"
#include "SL_ByteBuffer.h"
#include "common.h"
#include "anti-hotlinking-c.h"

#ifdef SOCKETLITE_USE_OPENSSL
    #include <openssl/aes.h>
#else
    #include "aes.h"
#endif

#define ANTI_HOTLINKING_C_VERSION 1
#if defined(ANTI_HOTLINKING_C_TYPE_PC)
#   define ANTI_HOTLINKING_C_TYPE "pc"
#   define ANIT_HOTLINKING_C_APP_KEY "fGVaJRM2WQpPoUlX5ZFly1yLiemJR7a9"
#elif defined(ANTI_HOTLINKING_C_TYPE_IOS)
#   define ANTI_HOTLINKING_C_TYPE "ios"
#   define ANIT_HOTLINKING_C_APP_KEY "nwVhv4EjrLm3mCQ1Z9qqmasEKdpiiDHt"
#elif defined(ANTI_HOTLINKING_C_TYPE_ANDRIOD)
#   define ANTI_HOTLINKING_C_TYPE "andriod"
#   define ANIT_HOTLINKING_C_APP_KEY "UIBOS8OIKph4QFuebdbhwnXY0PYeBpMd"
#elif defined(ANTI_HOTLINKING_C_TYPE_FLASH)
#   define ANTI_HOTLINKING_C_TYPE "flash"
#   define ANIT_HOTLINKING_C_APP_KEY "V8oo0Or1f047NaiMTxK123LMFuINTNeI"
#elif defined(ANTI_HOTLINKING_C_TYPE_HTML5)
#   define ANTI_HOTLINKING_C_TYPE "html5"
#   define ANIT_HOTLINKING_C_APP_KEY "7Bx7NJyyqS39qaEeOGzd2xd1uLMFoFxD"
#else
#   define ANTI_HOTLINKING_C_TYPE "none"
#   define ANIT_HOTLINKING_C_APP_KEY "nokey"
#endif

int anti_hotlinking_initialize_client()
{
    return 0;
}

int anti_hotlinking_uninitialize_client()
{
    return 0;
}

const char* anti_hotlinking_type()
{
    return ANTI_HOTLINKING_C_TYPE;
}

int anti_hotlinking_version()
{
    return ANTI_HOTLINKING_C_VERSION;
}

int anti_hotlinking_generate_signature(
    const char    *type,
    unsigned char random_hex[ANTI_HOTLINKING_CIPHER_BLOCK_HEX_SIZE],
    unsigned int  &random_hex_len,
    unsigned char signature_hex[ANTI_HOTLINKING_SIGNATURE_HEX_LEN],
    unsigned int  &signature_hex_len)
{
    if (random_hex_len < ANTI_HOTLINKING_CIPHER_BLOCK_HEX_SIZE)
    {
        return -1;
    }
    if (signature_hex_len < ANTI_HOTLINKING_SIGNATURE_HEX_LEN)
    {
        return -2;
    }

    char version_map_key[APP_VERSION_MAP_KEY_LEN + 1] = { 0 };
    int  version_map_key_len = SL_SNPRINTF(version_map_key, APP_VERSION_MAP_KEY_LEN, "%s-%d", 
        type, ANTI_HOTLINKING_C_VERSION);

    SL_ByteBuffer random(ANTI_HOTLINKING_CIPHER_BLOCK_SIZE + version_map_key_len + ANTI_HOTLINKING_APP_KEY_LEN);
    random.data_end(ANTI_HOTLINKING_CIPHER_BLOCK_SIZE);
    random.write(version_map_key, version_map_key_len);
    random.write(ANIT_HOTLINKING_C_APP_KEY, ANTI_HOTLINKING_APP_KEY_LEN);
    SL_Crypto_CRTRand rand;
    rand.random_byte(random.data(), ANTI_HOTLINKING_CIPHER_BLOCK_SIZE);

    unsigned char signature[ANTI_HOTLINKING_SIGNATURE_LEN] = { 0 };
    SL_Crypto_SHA256 sha256;
    sha256.final((const unsigned char *)random.data(), random.data_size(), signature);

    SL_Crypto_Hex hex;
    hex.set_upper_case(false);
    hex.encode((const unsigned char *)random.data(), ANTI_HOTLINKING_CIPHER_BLOCK_SIZE,
        random_hex, ANTI_HOTLINKING_CIPHER_BLOCK_HEX_SIZE);
    hex.encode(signature, ANTI_HOTLINKING_SIGNATURE_LEN, signature_hex, ANTI_HOTLINKING_SIGNATURE_HEX_LEN);

    random_hex_len    = ANTI_HOTLINKING_CIPHER_BLOCK_HEX_SIZE;
    signature_hex_len = ANTI_HOTLINKING_SIGNATURE_HEX_LEN;
    return 0;
}

int anti_hotlinking_decrypt(
    const char *sh,
    const char *st,
    const char *id,
    const char *bh,
    const char *iv_hex,
    const char *hmac_hex,
    const char *ke_hex,
    int flag,
    unsigned char k_hex[ANTI_HOTLINKING_K_HEX_LEN],
    unsigned int  &k_hex_len)
{
    return anti_hotlinking_decrypt2(2, sh, st, id, bh, iv_hex, hmac_hex, ke_hex, flag, k_hex, k_hex_len);
}

int anti_hotlinking_decrypt2(
    int type_id,
    const char *sh,
    const char *st,
    const char *id,
    const char *bh,
    const char *iv_hex,
    const char *hmac_hex,
    const char *ke_hex,
    int flag,
    unsigned char k_hex[ANTI_HOTLINKING_K_HEX_LEN],
    unsigned int  &k_hex_len)
{
    int ke_hex_len = strlen(ke_hex);
    if (ke_hex_len < ANTI_HOTLINKING_CIPHER_BLOCK_HEX_SIZE)
    {
        return -1;
    }
    if ((type_id < 0) || (type_id > 4))
    {
        return -2;
    }

    {
        static const char *APP_KEY[] = { 
            "fGVaJRM2WQpPoUlX5ZFly1yLiemJR7a9",     //0:pc
            "nwVhv4EjrLm3mCQ1Z9qqmasEKdpiiDHt",     //1:ios
            "UIBOS8OIKph4QFuebdbhwnXY0PYeBpMd",     //2:andriod
            "V8oo0Or1f047NaiMTxK123LMFuINTNeI",     //3:flash
            "7Bx7NJyyqS39qaEeOGzd2xd1uLMFoFxD"      //4:html5
        };

        SL_Crypto_Hex hex;
        hex.set_upper_case(false);

        int iv_hex_len = strlen(iv_hex);
        unsigned char iv[ANTI_HOTLINKING_CIPHER_BLOCK_SIZE] = { 0 };
        hex.decode((const unsigned char *)iv_hex, iv_hex_len, (unsigned char *)iv, ANTI_HOTLINKING_CIPHER_BLOCK_SIZE);
        unsigned char key[ANTI_HOTLINKING_APP_KEY_LEN] = { 0 };
        anti_hotlinking_generate_key(flag, sh, st, id, bh, (const char *)iv_hex, APP_KEY[type_id], key);

        unsigned char cipher_k1[ANTI_HOTLINKING_CIPHER_BLOCK_SIZE] = { 0 };
        hex.decode((const unsigned char *)ke_hex, ANTI_HOTLINKING_CIPHER_BLOCK_HEX_SIZE,
            cipher_k1, ANTI_HOTLINKING_CIPHER_BLOCK_SIZE);
        unsigned char k1[ANTI_HOTLINKING_CIPHER_BLOCK_SIZE] = { 0 };

#ifdef SOCKETLITE_USE_OPENSSL
        AES_KEY ctx;
        AES_set_decrypt_key(key, ANTI_HOTLINKING_APP_KEY_BITS, &ctx);
        AES_ecb_encrypt((const unsigned char *)cipher_k1, k1, &ctx, AES_DECRYPT);
        //AES_cbc_encrypt((const unsigned char *)cipher_k1, k1, ANTI_HOTLINKING_CIPHER_BLOCK_SIZE, 
        //    &ctx, (unsigned char *)iv, AES_DECRYPT);
#else
        aes_ctx ctx;
        aes_set_key(&ctx, key, ANTI_HOTLINKING_APP_KEY_BITS);
        aes_decrypt(&ctx, cipher_k1, k1);
#endif

        hex.encode((const unsigned char *)k1, ANTI_HOTLINKING_CIPHER_BLOCK_SIZE,
            k_hex, ANTI_HOTLINKING_CIPHER_BLOCK_HEX_SIZE);
        sl_memcpy(k_hex + ANTI_HOTLINKING_CIPHER_BLOCK_HEX_SIZE, ke_hex + ANTI_HOTLINKING_CIPHER_BLOCK_HEX_SIZE,
            ke_hex_len - ANTI_HOTLINKING_CIPHER_BLOCK_HEX_SIZE);
        k_hex_len = ke_hex_len;
    }

    return 0;
}

