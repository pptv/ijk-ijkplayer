#include "util.h"

static int find_stream_index(AVFormatContext* format_context, enum AVMediaType type);

int find_video_stream_index(AVFormatContext* format_context)
{
    return find_stream_index(format_context, AVMEDIA_TYPE_VIDEO);
}

int find_audio_stream_index(AVFormatContext* format_context)
{
    return find_stream_index(format_context, AVMEDIA_TYPE_AUDIO);
}

int find_stream_index(AVFormatContext* format_context, enum AVMediaType type)
{
    for (int i = 0; i < format_context->nb_streams; ++i) {
        if (format_context->streams[i]->codecpar->codec_type == type) {
            return i;
        }
    }

    return -1;
}
